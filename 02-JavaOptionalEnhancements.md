# Trading REST API Project Optional Enhancements

## Overview
These additions may be added to your project once you have a minimum viable version (MVP) working. The MVP should include a datetime field on your records.

## A) Unit Testing
Add unit/integration tests for your application. Include the jacoco dependency and make sure it's producing code coverage reports when you run the maven package goal.

## B) Further Endpoints
Consider adding some extra endpoints for enhanced functionality

* An endpoint to return all records for a given stock ticker. This may be implemented in a number of ways e.g.
    1. A completely separate endpoint.
    2. An optional parameter to the "findAll" endpoint.

* An endpoint to return all records with a given statusCode value. As above, this may be implemented in a number of different ways.

* An endpoint that returns the total number of stock that is currently held for a given ticker. Consider how you would handle both the 'BUY' and 'SELL' transactions.

* Warning, this one can be quite complex! An endpoint to return the aggregated cost of all of the stock you have for a given ticker. E.g. If there are a number of Buy and Sell transactions for a given ticker, what is the total aggregated cost per currently held stock.

## C) Add an Extra Database Table
It would be logical in such an application to include another database table that holds the history of changes to your records. In particular, we expect the statusCode field will be updated over time. It would be useful to have a table that keeps a record of any update that takes place for any of the transactions.

E.g. If the statusCode field of a transaction record changes from 0 to 1. This new table would have a row indicating the change that took place and the datetime at which it happened.

## D) A Second Spring Boot Application
The current application/service is keeping track of transactions on stocks. How would you keep track of transactions in currencies, bonds, crypto etc. These could be implemented as completely separate spring boot applications (services) in a microservice system OR you could adjust your existing service to also handle transactions for other financial instruments.


## Useful Links
* [Optional Parameters in Endpoints](https://www.baeldung.com/spring-request-param)
* [Matching Patterns in Spring Boot Mappings](https://www.baeldung.com/spring-5-mvc-url-matching)

