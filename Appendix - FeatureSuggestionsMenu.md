# Application Features

This is a menu of features that your team may consider implementing. These are **NOT** requirements, the purpose of these is to give ideas.

**Note:** "customer" and "admin" users are mentioned, for this proof of concept we can skip any authorization/authentication e.g. perhaps separate UI pages, but no requirement to authenticate.

## General Features

- Include JavaDoc for main classes, particular controller classes
- Ensure reasonable unit testing on all components, be prepared to show code coverage reports
- Have components deployed to Openshift through an automated pipeline


## Frontend Features
### As a user I would like...

- to issue new buy/sell orders
- to see a list of all my orders
- to be able to cancel orders if they have not been sent to an exchange (consider a status code to mark orders as cancelled rather than deleting them)
- to see a summary of all my positions/holdings (i.e. how much of what do I currently own)
- to see the current valuation of my portfolio
- to see the latest prices of stocks that I am interested in
- when issuing a new order, I would like the latest market price for this stock suggested by the UI

### As an admin I would like...

-  to see a log of all state changes to all orders and the times they occurred

## Backend Features

## As a user I would like...

- to keep track of cash held in various accounts / currencies. This cash holding could be checked when issuing buy/sell orders
- to schedule orders e.g. issue an order that should be triggered when a price goes above/below a requested price

## As a compliance officer I would like...

- Pre-trade validation: orders should have a price within a defined threshold of the previous days close. This could be implemented as an independent microservice.
- Pre-trade credit check: a customer should have enough cash in their account to cover a trade. This could be implemented as an independent microservice.
- Pre-trade risk: orders should pass appropriate compliance checks before being allowed to execute. E.g. a "block" list of stocks that are not authorised to be traded.

